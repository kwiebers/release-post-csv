# frozen_string_literal: true

require 'gitlab'
require_relative 'file_release'
require_relative 'directory_release'

Gitlab.private_token = ENV['GITLAB_PERSONAL_TOKEN']
Gitlab.endpoint = 'https://gitlab.com/api/v4'

# Constants used for API calls
WWW_GITLAB_COM_PATH = 'gitlab-com/www-gitlab-com'
RELEASE_POSTS_DIRECTORY_PATH = 'data/release_posts'
PER_PAGE = 100
GIT_REF = 'master'
FILENAME = ENV['CSV_FILENAME']

release_posts = Gitlab.tree(WWW_GITLAB_COM_PATH,
                            {
                              path: RELEASE_POSTS_DIRECTORY_PATH,
                              PER_PAGE: PER_PAGE,
                              ref: GIT_REF
                            }).auto_paginate

filtered_release_posts = release_posts.reject do |post|
  post.name == 'unreleased'
end
grouped_release_posts = filtered_release_posts.group_by(&:type)

releases = grouped_release_posts.map do |type, posts|
  case type
  when 'blob'
    FileRelease.process(posts)
  when 'tree'
    DirectoryRelease.process(posts)
  end
end.flatten.compact

releases.sort_by!(&:to_semantic_version)

# Build CSV
CSV.open(FILENAME, 'wb') do |csv|
  csv << %w[release base_items bug_items performance_items]
  releases.each do |release|
    csv << release.to_csv_row
  end
end
