# frozen_string_literal: true

require_relative 'release_info'
require 'yaml'

# Examine an individual Release File
class ReleaseFile
  BUG_FEATURES_TITLE = 'Bug fixes'
  PERFORMANCE_FEATURES_TITLE = 'Performance improvements'
  ITEM_LINE_REGEX = %r{^[-,*] \[.+\]\(https:\/\/gitlab.com\/.+\)(.+)?}.freeze
  URL_FIELDS_TO_COUNT = [
    'issue_url',
    'epic_url'
  ]

  attr_reader :path, :name, :type

  def initialize(path, name, type)
    puts "Extracting items from file: #{path}"

    @path = path
    @name = name
    @type = type

    @release_file_content ||=
      Gitlab.file_contents(WWW_GITLAB_COM_PATH, @path, GIT_REF)

    @release_yml ||= YAML.safe_load(@release_file_content)

    return unless valid_item?
  end

  %w[top_features primary_features secondary_features].each do |method|
    define_method method.to_s do
      if features_map[method.to_sym]
        features_map[method.to_sym] - (bug_features | performance_features)
      else
        []
      end
    end

    define_method "#{method}_count" do
      return 0 unless send(method.to_sym)

      count_feature_items(send(method.to_sym))
    end
  end

  %w[bug_features performance_features].each do |method|
    define_method method.to_s do
      features.select do |feature|
        feature['name'] == self.class.const_get("#{method.upcase}_TITLE")
      end
    end

    define_method "#{method}_count" do
      count_description_items(send(method.to_sym))
    end
  end

  def main_feature_count
    top_features_count + primary_features_count + secondary_features_count
  end

  def to_release_info
    if !valid_item?
      default_counts
    else
      ReleaseInfo.new(name, main_feature_count, bug_features_count,
                      performance_features_count)
    end
  end

  private

  def default_counts
    ReleaseInfo.new(name, 0, 0, 0)
  end

  def features_map
    @features_map ||=
      {
        top_features: @release_yml['features']['top'],
        primary_features: @release_yml['features']['primary'],
        secondary_features: @release_yml['features']['secondary']
      }
  end

  def features
    features_map.values.compact.flatten
  end

  def count_feature_items(items)
    with_links = items.reject do |item|
      (URL_FIELDS_TO_COUNT & item.keys).length.zero?
    end

    without_links = items - with_links

    count_feature_without_link_field(without_links) +
    count_feature_with_link_field(with_links)
  end

  def count_feature_without_link_field(items)
    # count description items only for those without links
    items.map do |item|
      descripton_count = count_description_items([item])
      # if no descripton items only count the top level item
      descripton_count.zero? ? 1 : descripton_count
    end.sum
  end

  def count_feature_with_link_field(items)
    # for those with links count the link and description items
    items.length + count_description_items(items)
  end

  def count_description_items(sub_items)
    sub_items.map do |sub_item|
      puts "Counting sub items for #{sub_item['name']}"
      lines = sub_item['description'].split(/(\n)/)
      lines.select do |item_line|
        item_line =~ ITEM_LINE_REGEX
      end.length
    end.sum
  end

  def valid_item?
    @type == 'tree' && !@release_yml['features'] ? false : true
  end
end
