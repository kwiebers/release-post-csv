# frozen_string_literal: true

require 'semantic'

# Counts for a Release
class ReleaseInfo
  attr_accessor :name
  attr_reader :item_count, :bug_count, :performance_count

  def initialize(name, item_count, bug_count, performance_count)
    @name = name
    @item_count = item_count
    @bug_count = bug_count
    @performance_count = performance_count
  end

  def to_csv_row
    ["\"#{name.gsub('_', '.')}\"", item_count, bug_count, performance_count]
  end

  def to_semantic_version
    Semantic::Version.new("#{name.gsub('_', '.')}.0")
  end
end
